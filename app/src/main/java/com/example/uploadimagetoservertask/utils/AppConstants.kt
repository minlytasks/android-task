package com.example.uploadimagetoservertask.utils

object AppConstants {
    const val baseUrl = "http://192.168.1.3:4000"
    const val mediaUrl = "$baseUrl/photo/"
}