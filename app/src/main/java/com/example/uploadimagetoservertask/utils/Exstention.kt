package com.example.uploadimagetoservertask.utils

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

fun String.createMultiPartBody(): MultipartBody.Part {
    val file = File(this)
    val requestBody = file.createRequestBody()
    return MultipartBody.Part.createFormData(
        name = "photo",
        filename = file.name,
        body = requestBody
    )
}

fun File.createRequestBody(): RequestBody = RequestBody.create("image/*".toMediaTypeOrNull(), this)