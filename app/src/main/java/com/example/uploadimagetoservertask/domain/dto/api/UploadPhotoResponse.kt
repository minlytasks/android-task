package com.example.uploadimagetoservertask.domain.dto.api

data class UploadPhotoResponse(
    val photo_url: String
)