package com.example.uploadimagetoservertask.domain.dto.api

data class PhotoResponse(
    val children: List<Children>,
    val name: String,
    val path: String,
    val size: Int,
    val type: String
)

data class Children(
    val extension: String,
    val name: String,
    val path: String,
    val size: Int,
    val type: String
)