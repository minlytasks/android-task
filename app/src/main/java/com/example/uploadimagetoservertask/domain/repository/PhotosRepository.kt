package com.example.uploadimagetoservertask.domain.repository

import com.example.uploadimagetoservertask.data.PhotosDataSource
import com.example.uploadimagetoservertask.data.remote.PhotosApiService
import com.example.uploadimagetoservertask.domain.dto.APIResult
import okhttp3.MultipartBody
import okhttp3.ResponseBody

class PhotosRepository(
    private val photosApiService: PhotosApiService
) : BaseRepository(), PhotosDataSource {
    override suspend fun getPhotos(): APIResult<ResponseBody> {
        return getAPIResult(safeApiCall { photosApiService.getPhotos() })
    }

    override suspend fun uploadPhoto(photo: MultipartBody.Part): APIResult<ResponseBody> {
        return getAPIResult(safeApiCall { photosApiService.uploadPhoto(photo) })
    }
}