package com.example.uploadimagetoservertask.data.remote

import com.example.uploadimagetoservertask.utils.AppConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Api {
    private val okHttpClient = OkHttpClient().newBuilder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }).build()

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(AppConstants.baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    object Companion {
        fun photosApiService(): PhotosApiService = retrofit()
            .create(PhotosApiService::class.java)
    }
}