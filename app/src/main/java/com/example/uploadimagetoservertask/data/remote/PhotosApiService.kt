package com.example.uploadimagetoservertask.data.remote

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface PhotosApiService {
    @GET(ApiEndPoint.PHOTOS)
    suspend fun getPhotos(): Response<ResponseBody>

    @Multipart
    @POST(ApiEndPoint.UPLOAD)
    suspend fun uploadPhoto(
        @Part photo: MultipartBody.Part
    ): Response<ResponseBody>
}