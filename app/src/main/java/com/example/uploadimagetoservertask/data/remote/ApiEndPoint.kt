package com.example.uploadimagetoservertask.data.remote

object ApiEndPoint {
    const val PHOTOS = "photos"
    const val UPLOAD = "upload"
}