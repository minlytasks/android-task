package com.example.uploadimagetoservertask.data

import com.example.uploadimagetoservertask.domain.dto.APIResult
import okhttp3.MultipartBody
import okhttp3.ResponseBody

interface PhotosDataSource {
    suspend fun getPhotos(): APIResult<ResponseBody>

    suspend fun uploadPhoto(photo: MultipartBody.Part): APIResult<ResponseBody>
}