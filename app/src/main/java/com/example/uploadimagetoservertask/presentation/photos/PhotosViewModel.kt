package com.example.uploadimagetoservertask.presentation.photos

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.uploadimagetoservertask.data.PhotosDataSource
import com.example.uploadimagetoservertask.domain.dto.APIResult
import com.example.uploadimagetoservertask.domain.dto.api.PhotoResponse
import com.example.uploadimagetoservertask.domain.dto.api.UploadPhotoResponse
import com.example.uploadimagetoservertask.presentation.base.BaseViewModel
import com.example.uploadimagetoservertask.utils.AppConstants
import com.example.uploadimagetoservertask.utils.createMultiPartBody
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

class PhotosViewModel(
    private val photosDataSource: PhotosDataSource
) : BaseViewModel() {
    val photosLiveData: MutableLiveData<List<PhotoDataItem>> = MutableLiveData()
    var photosList: MutableList<PhotoDataItem> = mutableListOf()

    init {
        getPhotos()
    }

    private fun getPhotos() {
        isLoading.value = true
        viewModelScope.launch {
            when (val result = photosDataSource.getPhotos()) {
                is APIResult.Success -> handleSuccessResponse(result)
                is APIResult.Error -> handleErrorResponse(result)
            }
            isLoading.value = false
        }
    }

    private fun handleSuccessResponse(result: APIResult.Success<ResponseBody>) {
        try {
            val type = object : TypeToken<PhotoResponse>() {}.type
            val baseResponse = Gson().fromJson<PhotoResponse>(
                result.baseResponse.string(),
                type
            )
            Log.d("TAG", "handleSuccessResponse: $baseResponse")
            photosList = baseResponse.children.map {
                PhotoDataItem(
                    AppConstants.mediaUrl + it.name
                )
            }.toMutableList()
            photosLiveData.value = photosList
        } catch (ex: Exception) {
            showToast.value = ex.localizedMessage
            return
        }
    }

    fun uploadPhoto(filePath: String) {
        isLoading.value = true
        viewModelScope.launch {
            when (val result = photosDataSource.uploadPhoto(filePath.createMultiPartBody())) {
                is APIResult.Success -> handleSuccessUploadPhotoResponse(result)
                is APIResult.Error -> handleErrorResponse(result)
            }
            isLoading.value = false
        }
    }

    private fun handleSuccessUploadPhotoResponse(result: APIResult.Success<ResponseBody>) {
        try {
            val type = object : TypeToken<UploadPhotoResponse>() {}.type
            val baseResponse = Gson().fromJson<UploadPhotoResponse>(
                result.baseResponse.string(),
                type
            )
            Log.d("TAG", "handleSuccessUploadPhotoResponse: $baseResponse")
            val photoUrl = baseResponse.photo_url.replace("localhost", "192.168.1.3")
            photosList.add(PhotoDataItem(photoUrl))
            photosLiveData.value = photosList
        } catch (ex: Exception) {
            showToast.value = ex.localizedMessage
            return
        }
    }
}