package com.example.uploadimagetoservertask.presentation.photos

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.uploadimagetoservertask.BR
import com.example.uploadimagetoservertask.R
import com.example.uploadimagetoservertask.databinding.PhotosFragmentBinding
import com.example.uploadimagetoservertask.presentation.base.BaseFragment
import com.example.uploadimagetoservertask.utils.ChoosePhotoHelper
import org.koin.androidx.viewmodel.ext.android.viewModel

class PhotosFragment : BaseFragment<PhotosFragmentBinding, PhotosViewModel>() {
    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.photos_fragment
    override val viewModel: PhotosViewModel by viewModel()
    private lateinit var choosePhotoHelper: ChoosePhotoHelper

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val photosAdapter = PhotosAdapter(arrayListOf())
        getViewDataBinding().photosRecyclerView.adapter = photosAdapter
        getViewDataBinding().fab.setOnClickListener { choosePhotoHelper.showChooser() }
        choosePhotoHelper = ChoosePhotoHelper.with(this)
            .asFilePath()
            .withState(savedInstanceState)
            .build { filePath -> filePath?.let { viewModel.uploadPhoto(it) } }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        choosePhotoHelper.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        choosePhotoHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        choosePhotoHelper.onSaveInstanceState(outState)
    }
}