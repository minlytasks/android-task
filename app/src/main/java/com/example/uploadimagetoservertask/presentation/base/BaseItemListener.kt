package com.example.uploadimagetoservertask.presentation.base

interface BaseItemListener<T> {
    fun onItemClick(item: T)
}