package com.example.uploadimagetoservertask.presentation.photos

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.uploadimagetoservertask.databinding.PhotoItemBinding
import com.example.uploadimagetoservertask.presentation.base.BaseRecyclerViewAdapter
import com.example.uploadimagetoservertask.presentation.base.BaseViewHolder

class PhotosAdapter(items: MutableList<PhotoDataItem>) :
    BaseRecyclerViewAdapter<PhotoDataItem>(items) {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return CardViewHolder(
            PhotoItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    inner class CardViewHolder(private val mBinding: PhotoItemBinding) :
        BaseViewHolder(mBinding.root) {
        override fun onBind(position: Int) {
            val photoDataItem = items[position]
            mBinding.photoDataItem = photoDataItem
            mBinding.executePendingBindings()
        }
    }
}