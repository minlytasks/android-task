package com.example.uploadimagetoservertask.presentation.base

import android.os.Bundle
import androidx.lifecycle.ViewModel
import com.example.uploadimagetoservertask.domain.dto.APIResult
import com.example.uploadimagetoservertask.utils.SingleLiveEvent
import kotlin.reflect.KClass

abstract class BaseViewModel : ViewModel() {
    val navigationCommand: SingleLiveEvent<NavigationCommand> = SingleLiveEvent()
    val activityToStart = SingleLiveEvent<Pair<KClass<*>, Bundle?>>()
    val isLoading: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val showToast: SingleLiveEvent<String> = SingleLiveEvent()

    fun handleErrorResponse(result: APIResult.Error) {
        try {

        } catch (ex: Exception) {
            showToast.value = ex.localizedMessage
        }
    }
}