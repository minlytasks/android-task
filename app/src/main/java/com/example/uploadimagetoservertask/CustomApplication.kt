package com.example.uploadimagetoservertask

import androidx.multidex.MultiDexApplication
import com.example.uploadimagetoservertask.data.PhotosDataSource
import com.example.uploadimagetoservertask.data.remote.Api
import com.example.uploadimagetoservertask.domain.repository.PhotosRepository
import com.example.uploadimagetoservertask.presentation.MainViewModel
import com.example.uploadimagetoservertask.presentation.photos.PhotosViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel

class CustomApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        val myModule = module {
            single { Api.Companion.photosApiService() }

            viewModel { MainViewModel() }
            viewModel { PhotosViewModel(get()) }

            single<PhotosDataSource> { PhotosRepository(get()) }
        }

        startKoin {
            androidContext(this@CustomApplication)
            modules(listOf(myModule))
        }
    }
}